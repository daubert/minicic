Small prototype to discuss particle-grid interactions, using CIC as a discussion basis.

Setup:
create two repos obj/ and data/

To compile :
make

To check results :
use the Check.ipynb jupyter notebook in the utils repo
the wiki has some examples of results

To change parameters:
in src/main.cpp 
the grid size can be modifed using the L parameter
the number of particle can be modified using the NP parameter

