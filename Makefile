DEFINES =
#DEFINES+= -DNGP

#===============================================================================
# Compilers
#===============================================================================

CXX= g++
CXX_FLAGS=  -g -O2 -Wall  #-ftree-vectorize -ffast-math -fno-cx-limited-range -O3 -Wimplicit -g
CXX_LIBS= #-lm #-fopenmp -lstdc++

CXX_OBJS= main.o particle.o morton.o cell.o


#===============================================================================
# Compile
#===============================================================================

OBJDIR = ./obj
SRCDIR = ./src

$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXX_FLAGS) $(CXX_LIBS) $(DEFINES) -c $< -o $@

EXECUTABLE = minicic

OBJ=$(patsubst %,$(OBJDIR)/%,$(CXX_OBJS))

LCC=$(CXX)

all:$(OBJ)
	$(LCC) $(OBJ) $(LD_DIR) $(CXX_FLAGS) $(CXX_LIBS) -o $(EXECUTABLE) $(LD_FLAGS)

clean:
	rm -f $(OBJDIR)/*.o $(EXECUTABLE) *~
