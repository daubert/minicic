#pragma once
#include "morton.h"
#include <iostream>
using namespace std;

//-------  a class to define particles
class Particle:public mortonkey{
  // inherits mortonkey properties
private:
  int index; // index for particle tracking
  double xyz[3]; // position [0...1.]
  double mass; // mass
  double quant; // proxy for cell2part
public:
  void setindex(int i);
  void setxyz(double x[], double dx, unsigned int l);
  void getxyz(double x[]);
  double getmass();
  void setmass(double m);
  void setquant(double q);
  double getquant();
  void dump(ofstream &pfile);
};
// --------------------------------------
