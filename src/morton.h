#pragma once

//===========================================
//===========================================

// Morton key Object, inherited by particles and cells
class mortonkey{
  private:
    unsigned long key; // the extended morton key

  public :
    unsigned long getkey(); // getter for key
    void C2M(unsigned int x[], unsigned int l); // Cartesian to Morton
    unsigned int M2C(unsigned int x[]); // Morton to Cartesian
    void nei27(mortonkey neikey[]);
    // Operator overload
    bool operator==(const mortonkey& m) const
	   {
		     return key == m.key;
	   }

     bool operator<(const mortonkey& m) const
      {
          return key < m.key;
      }

      bool operator>(const mortonkey& m) const
       {
           return key > m.key;
       }
};

//===========================================
//===========================================

template <typename T>
int compt(const void * a, const void * b){
  // comparion function for bsearch
  T *oa;
  T *ob;

  oa=(T *) a;
  ob=(T *) b;

  int res=0;
  if(*oa>*ob){
    res=1;
  }
  else if(*oa<*ob){
    res=-1;
  }
  return res;
}

//===========================================
//===========================================

template <typename T>
T* lookup(unsigned long *key, T *set, unsigned int nset){
  // wrapper for the bsearch function
  // looking for the first element in set (size nset) with a given key
  T *target;
  target=(T *)bsearch(key,set,nset,sizeof(T),compt<T>);

#if 1
  // rewind in case of multiple occurences
  if((target)&&(target>set)){
    while((target-1)->getkey()==*key){
      target--;
      if(target==set) break;
    }
  }
#endif
  return target;
}
