#include "morton.h"
#include "particle.h"
#include <iostream>
using namespace std;

//-------  a class to define particles
class Cell:public mortonkey{
  // inherits mortonkeys properties
private:
  unsigned int ijk[3]; // cartesian index [0..N]
  double density;
public:
  void setijk(unsigned int ijk[], unsigned int level);
  void part2cell(Particle *p, double dx);
  void cell2part(Particle *p, double dx);
  double getdensity();
  void setdensity(double d);
  void dump(ofstream &cfile);
};
// --------------------------------------
