#include <iostream>
#include "particle.h"
#include <fstream>
using namespace std;

// ======================================================
void Particle::setindex(int i){
  this->index=i;
}

// ======================================================
void Particle::setxyz(double x[], double dx, unsigned int l){
  int i;
  unsigned int ix[3];
  for(i=0;i<3;i++){
    this->xyz[i]=x[i];
    ix[i]=(unsigned int)(x[i]/dx);
  }
  this->C2M(ix,l);
}


void Particle::getxyz(double x[]){
  int i;
  for(i=0;i<3;i++) x[i]=this->xyz[i];
}

double Particle::getmass(){
  return this->mass;
}

void Particle::setmass(double m){
  this->mass=m;
}

void Particle::setquant(double q){
  this->quant=q;
}

double Particle::getquant(void){
  return this->quant;
}

void Particle::dump(ofstream &partfile){

  unsigned int xbis[3];
  int j;

  for(j=0;j<3;j++){
    partfile<<this->xyz[j]<<" ";
  }

  partfile<<this->getkey()<<" ";
  this->M2C(xbis);
  for(j=0;j<3;j++){
    partfile<<xbis[j]<<" ";
  }

  partfile<<this->quant<<" ";

  partfile<<endl;
}
