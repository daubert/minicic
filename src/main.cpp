#include <iostream>
#include <fstream>
#include <algorithm>    // std::sort
#include <math.h>
#include "particle.h"
#include "cell.h"
#include "morton.h"
#include <cstdlib>
#include <vector>

using namespace std;

// g++ cell.cpp particle.cpp morton.cpp main.cpp

// ================ PARAMETERS ========================
#define NP 100000 // The Number of particles to generate
#define L 7 // The grid resolution 2**L x 2**L x 2**L

//========================================

int main() {

  // ============= The data ==============
  vector<Particle> part;
  vector<Cell> cell;

  // Ancillary data ==================

  Cell c;
  Particle p;

  double pos[3];
  unsigned int x[3];
  int i,j,k;
  Particle *pcur;

  int inei;
  Cell *lcell;    
  mortonkey neikey[27];

  // ==== some derived parameters values ========

  int nc=1<<L; // number of cells (linear)
  double dx=1.0/nc;

  cout<<"Number of linear cells "<<nc<<endl;

  // ================ CELLS    =========================
  cout<<"Generating Coarse grid"<<endl;
  for(i=0;i<nc;i++){
    for(j=0;j<nc;j++){
      for(k=0;k<nc;k++){

        x[0]=i;
        x[1]=j;
        x[2]=k;

        c.setijk(x,L);
        c.setdensity(0.);
        cell.push_back(c);
      }
    }
  }


  //================ PARTICLES =========================


#ifdef ICCUBE
  double ex[]={0.3,0.2,0.1};
  cout<<"Generating particles in CUBE"<<endl;
  for(i=0;i<NP;i++){
    for(j=0;j<3;j++) pos[j]=ex[j]*((rand()/(double)RAND_MAX)-0.5)+0.5;
    p.setxyz(pos,dx,L);
    p.setmass(1./NP);
    part.push_back(p);
    p.setquant(0.);
  }
#else
  cout<<"Generating particles in SPHERE"<<endl;
  for(i=0;i<NP;i++){

    double th=acos(rand()/(double)RAND_MAX);
    double ph=rand()/(double)RAND_MAX*2*M_PI;
    double r= rand()/(double)RAND_MAX*0.2;

    pos[0]=r*sin(th)*cos(ph)+0.5;
    pos[1]=r*sin(th)*sin(ph)+0.5;
    pos[2]=r*cos(th)+0.5;

    p.setxyz(pos,dx,L);
    p.setmass(1./NP);
    p.setquant(0.);
    part.push_back(p);
  }
#endif


  // ===========  sorting part and cells according to their key
  cout<<"Sort PArticles"<<endl;
  sort(part.begin(),part.end());
  cout<<"Sort Cells"<<endl;
  sort(cell.begin(),cell.end());


  unsigned int psize=part.size();
  unsigned int csize=cell.size();

  // =========  part2cell : CIC Case
  cout<<"CIC Projection"<<endl;

  for(vector<Cell>:: iterator c=cell.begin();c!=cell.end();c++){
    //if(i%1000==0) cout<<"toto "<<i<<endl;

    //getting the 27 neighbours
    c->nei27(neikey);

    // Looking for the first particle
    unsigned long keyc=c->getkey();
    pcur=lookup(&keyc,&part[0],psize); // Bsearch of sorted part

    if(c==cell.begin()) cout<<"neighbours in particles"<<endl;

    // scanning the particles
    if(pcur){
      while(*pcur==*c){
	// scanning the neighbours
	for(inei=0;inei<27;inei++){
#ifdef NGP
	  if(inei!=13) continue;
#endif
	  // looking for neighbor
	  unsigned long keyn=neikey[inei].getkey();
	  lcell=lookup(&keyn,&cell[0],csize);
	  if(lcell){
	    lcell->part2cell(pcur,dx);
	  }
	}
	pcur++;
      }
    }
  }
  // end scan cells


  // ========== Cell2part
  // Now let's assigne a eulerian value to PARTICLES
  
  cout<<"CIC Interpolation"<<endl;
  
  for(vector<Cell>:: iterator c=cell.begin();c!=cell.end();c++){
    
    //getting the 27 neighbours
    c->nei27(neikey);
    
    // Looking for the first particle
    unsigned long keyc=c->getkey();
    pcur=lookup(&keyc,&part[0],psize); // Bsearch of sorted part
    
    if(c==cell.begin()) cout<<"neighbours in particles"<<endl;
    
    // scanning the particles
    if(pcur){
      while(*pcur==*c){
	// scanning the neighbours
	for(inei=0;inei<27;inei++){
#ifdef NGP
	  if(inei!=13) continue;
#endif
	  // looking for neighbor
	  unsigned long keyn=neikey[inei].getkey();
	  lcell=lookup(&keyn,&cell[0],csize);
	  if(lcell){
	    lcell->cell2part(pcur,dx);
	  }
	}
	pcur++;
      }
    }
  }


  // ============== I/O ========================
  ofstream partfile;
  ofstream cellfile;

  partfile.open("data/part.dat", ios::out | ios::trunc );
  cellfile.open("data/cell.dat", ios::out | ios::trunc );

  //  ============ some IO for particles
  cout<<"I/O part"<<endl;
  for(vector<Particle>:: iterator p= part.begin();p!=part.end();p++){
      p->dump(partfile);
  }

  // =======================  I/O CELLS

  cout<<"I/O cell"<<endl;
  for(vector<Cell>:: iterator c=cell.begin();c!=cell.end();c++){
    c->dump(cellfile);
  }



  partfile.close();
  cellfile.close();
  return 0;
}
