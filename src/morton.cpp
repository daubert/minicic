#include "morton.h"

//=============================================
unsigned long bitscat(unsigned int a)
{
  unsigned long x;
  x=a;
  x = x& 0x1fffff;
  x = (x | x << 32) & 0x1f00000000ffff;
  x = (x | x << 16) & 0x1f0000ff0000ff;
  x = (x | x << 8) & 0x100f00f00f00f00f;
  x = (x | x << 4) & 0x10c30c30c30c30c3;
  x = (x | x << 2) & 0x1249249249249249;


  return x;
}

//=============================================
unsigned int bitgat(unsigned long x)
{

  x = x& 0x1249249249249249;;
  x = (x ^ (x >> 2)) & 0x10c30c30c30c30c3;
  x = (x ^ (x >> 4)) & 0x100f00f00f00f00f;
  x = (x ^ (x >> 8)) & 0x1f0000ff0000ff;
  x = (x ^ (x >> 16)) & 0x1f00000000ffff;
  x = (x ^ (x >> 32)) & 0x1fffff;

  return (unsigned int) x;
}

//============================================
unsigned int get_level(unsigned long c){
  // returns the level encoded in c
  unsigned int level;
  for (level=0; c!=1; c>>=3, level++);
  return level;
}

//=============================================
void mortonkey::C2M(unsigned int x[], unsigned int level){
  // Cartesian to Morton, assuming a level
  unsigned long c;
  c=bitscat(x[0])| (bitscat(x[1])<<1)|bitscat(x[2])<<2;
  c|=(1<<(3*level));
  this->key=c;
}

//============================================
unsigned int mortonkey::M2C(unsigned int x[]){
  // Morton to Cartesian, returns the level and cart. coord in x
  unsigned long c=this->key;
  unsigned int level=get_level(this->key);

  c &= ~(1 << (3*(level)));
  x[0]= bitgat(c);
  x[1]=bitgat(c>>1);
  x[2]=bitgat(c>>2);
  return level;
}

//===========================================
unsigned long mortonkey::getkey(void){
  return this->key;
}

//===========================================
void mortonkey::nei27(mortonkey neikey[]){
  unsigned int x[3],xx[3];
  unsigned int level;

  level=this->M2C(x);

  unsigned int dmax=(1<<level)-1; //2**level-1                                                                           
  int di;
  int dj;
  int dk;

  int count=0;
  for(dk=-1;dk<=1;dk++)
    {
      for(dj=-1;dj<=1;dj++)
        {
          for(di=-1;di<=1;di++)
            {

              xx[0]=(x[0]+di<0?dmax:(x[0]+di>dmax?0:x[0]+di));
              xx[1]=(x[1]+dj<0?dmax:(x[1]+dj>dmax?0:x[1]+dj));
              xx[2]=(x[2]+dk<0?dmax:(x[2]+dk>dmax?0:x[2]+dk));

	      neikey[count].C2M(xx,level);
              count++;
            }
        }
    }
}
