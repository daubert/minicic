#include "cell.h"
#include "particle.h"
#include <fstream>

// =====================================================
void Cell::part2cell(Particle *p, double dx){
  // projection function, 
  double w=1.0;
#ifndef NGP
  double xp[3];
  int i;
  p->getxyz(xp);
  for(i=0;i<3;i++){
    double d=xp[i]/dx-this->ijk[i];
    w*=(d>=1.?0.:(d<=-1.?0.:(d<0.?1.+d:1.-d)));
  }
#endif	    
  this->density+=w*p->getmass()/(dx*dx*dx);
}

// =====================================================
void Cell::cell2part(Particle *p, double dx){
  // interpolation function, to be extended to CIC

  double w=1.0;
  double quant;
#ifndef NGP
  double xp[3];
  int i;
  p->getxyz(xp);
  for(i=0;i<3;i++){
    double d=xp[i]/dx-this->ijk[i];
    w*=(d>=1.?0.:(d<=-1.?0.:(d<0.?1.+d:1.-d)));
  }
#endif

  quant=p->getquant();
  p->setquant(quant+w);
}

// =====================================================
void Cell::setijk(unsigned int ijk[], unsigned int l){
  int i;
  for(i=0;i<3;i++){
    this->ijk[i]=ijk[i];
  }
  this->C2M(ijk,l);
}

void Cell::setdensity(double d){
  this->density=d;
}

double Cell::getdensity(void){
  return this->density;
}

//=====================================================
void Cell::dump(ofstream &cfile){
  int j;
  //this->M2C(ijk);
  for(j=0;j<3;j++){
    cfile<<this->ijk[j]<<" ";
  }
  cfile<<this->getkey()<<" "<<this->density<<endl;
}
